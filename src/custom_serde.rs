// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::queue::Queue;
use crate::sync_queue::SyncQueue;
use serde::de::Deserializer;
use serde::ser::Serializer;
use serde::{Deserialize, Serialize};

impl<'de, V> Deserialize<'de> for SyncQueue<V>
where
    V: 'de + Deserialize<'de> + Clone,
{
    /// Deserialize the queue.
    ///
    /// Queue and SyncQueue share the same representation
    /// so it is fine to (de)serialize from any of them.
    ///
    /// ```
    /// use squeue::SyncQueue;
    ///
    /// let export = "{\"capacity\":7,\"data\":[1,4,3]}";
    ///
    /// let queue: SyncQueue<usize> = serde_json::from_str(&export).unwrap();
    ///
    /// assert_eq!(3, queue.len());
    /// assert_eq!(7, queue.capacity());
    /// assert_eq!(Some(3), queue.pop());
    /// assert_eq!(Some(4), queue.pop());
    /// assert_eq!(Some(1), queue.pop());
    /// assert_eq!(None, queue.pop());
    /// ```
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let queue: Queue<V> = Queue::deserialize(deserializer)?;
        Ok(SyncQueue::from(queue))
    }
}

impl<V> Serialize for SyncQueue<V>
where
    V: Serialize + Clone,
{
    /// Serialize the sync queue.
    ///
    /// This is possibly long, and requires data to support Clone.
    /// Internally, extracts non-sync version and serializes it.
    ///
    /// ```
    /// use squeue::SyncQueue;
    /// use serde_json::json;
    ///
    /// let queue = SyncQueue::new(10);
    ///
    /// queue.push(7);
    /// queue.push(22);
    ///
    /// let export = json!(&queue).to_string();
    ///
    /// assert_eq!("{\"capacity\":10,\"data\":[22,7]}", export);
    /// ```
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let queue = Queue::<V>::from(self.clone()).serialize(serializer)?;
        Ok(queue)
    }
}
