// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

//! [Squeue](https://gitlab.com/liberecofr/squeue) is a sized queue.
//!
//! This is just a simple wrapper over a [standard Rust VecDeque](https://doc.rust-lang.org/stable/std/collections/struct.VecDeque.html).
//!
//! ![Squeue icon](https://gitlab.com/liberecofr/squeue/raw/main/squeue.png)
//!
//! # Examples
//!
//! Basic queue:
//!
//! ```
//! use squeue::Queue;
//!
//! let mut queue: Queue<usize> = Queue::new(3);
//! assert_eq!(None, queue.push(1));
//! assert_eq!(None, queue.push(2));
//! assert_eq!(None, queue.push(3));
//! assert_eq!(Some(1), queue.push(4));
//! assert_eq!(3, queue.len());
//! ```
//!
//! Thread-safe queue:
//!
//! ```
//! use squeue::SyncQueue;
//! use std::thread;
//!
//! let queue: SyncQueue<&str> = SyncQueue::new(50);
//!
//! let queue1 = queue.clone();
//! let handle1 = thread::spawn(move || queue1.push("x"));
//! let queue2 = queue.clone();
//! let handle2 = thread::spawn(move || queue2.push("y"));
//!
//! handle1.join().unwrap();
//! handle2.join().unwrap();
//!
//! assert_eq!(2, queue.len());
//! ```

#[cfg(feature = "serde")]
mod custom_serde;
mod drain;
mod queue;
mod sync_queue;

#[cfg(feature = "serde")]
pub use custom_serde::*;
pub use drain::*;
pub use queue::*;
pub use sync_queue::*;
