#![feature(test)]
extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use squeue::{Queue, SyncQueue};
    use std::collections::VecDeque;
    use test::Bencher;

    const CAPACITY: usize = 10_000;

    #[bench]
    fn bench_write_usize_squeue_queue(b: &mut Bencher) {
        let mut queue = Queue::new(CAPACITY);
        let mut i: isize = 0;
        b.iter(|| {
            queue.push(i);
            i += 1;
        });
    }

    #[bench]
    fn bench_write_usize_squeue_sync_queue(b: &mut Bencher) {
        let queue = SyncQueue::new(CAPACITY);
        let mut i: isize = 0;
        b.iter(|| {
            queue.push(i);
            i += 1;
        });
    }

    #[bench]
    fn bench_write_usize_builtin_vecdeque(b: &mut Bencher) {
        let mut queue = VecDeque::with_capacity(CAPACITY);
        let mut i: isize = 0;
        b.iter(|| {
            queue.push_front(i);
            i += 1;
        });
    }

    #[bench]
    fn bench_read_usize_squeue_queue(b: &mut Bencher) {
        let mut queue = Queue::new(CAPACITY);
        for i in 0..CAPACITY {
            queue.push(i);
        }
        b.iter(|| {
            queue.pop();
        });
    }

    #[bench]
    fn bench_read_usize_squeue_sync_queue(b: &mut Bencher) {
        let queue = SyncQueue::new(CAPACITY);
        for i in 0..CAPACITY {
            queue.push(i);
        }
        b.iter(|| {
            queue.pop();
        });
    }

    #[bench]
    fn bench_read_usize_builtin_vecdeque(b: &mut Bencher) {
        let mut queue = VecDeque::with_capacity(CAPACITY);
        for i in 0..CAPACITY {
            queue.push_front(i);
        }
        b.iter(|| {
            queue.pop_back();
        });
    }
}
